package eu.darkh.library.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Aleksander_Braula on 18-Jan-15.
 */
public class StorageUtils {

    private static SharedPreferences prefs = null;

    public static void initStorageUtils(Context context, String sharedPreferencesFile) {
        prefs = context.getSharedPreferences(sharedPreferencesFile, Context.MODE_PRIVATE);
    }

    public static void putStringInSharedPreferences(String key, String value) {
        SharedPreferences.Editor editor = prefs.edit();

        editor.putString(key, value);
        editor.apply();
    }

    public static void putIntInSharedPreferences(String key, int value) {
        SharedPreferences.Editor editor = prefs.edit();

        editor.putInt(key, value);
        editor.apply();
    }

    public static void putBooleanInSharedPreferences(String key, boolean value) {
        SharedPreferences.Editor editor = prefs.edit();

        editor.putBoolean(key, value);
        editor.apply();
    }

    public static String getStringFromSharedPreferences(String key) {
        return prefs.getString(key, "");
    }

    public static int getIntFromSharedPreferences(String key) {
        return prefs.getInt(key, 0);
    }

    public static boolean getBooleanFromSharedPreferences(String key) {
        return prefs.getBoolean(key, false);
    }

    public static String getStringResource(int id) {
        return UiUtils.getContext().getResources().getString(id);
    }
}

