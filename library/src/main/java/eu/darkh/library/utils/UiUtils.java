package eu.darkh.library.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.util.TypedValue;
import android.view.View;
import android.widget.Toast;

import eu.darkh.library.R;

/**
 * Created by Aleksander_Braula on 09-Jan-15.
 */
public class UiUtils {

    private static Context context;

    public static void setContext(Context baseContext) {
        UiUtils.context = baseContext;
    }

    public static Context getContext() {
        return UiUtils.context;
    }

    public static int convertIntToDp(int number) {
        Resources resources = context.getResources();
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, number, resources.getDisplayMetrics());
    }

    public static void showToastWithText(String text) {
        Toast toast = Toast.makeText(context, text, Toast.LENGTH_LONG);
        toast.show();
    }

    public static void showToastWithText(int stringId) {
        Toast toast = Toast.makeText(context, stringId, Toast.LENGTH_LONG);
        toast.show();
    }

    public static void showAlertDialogWithView(String title, String body, View view, DialogInterface.OnClickListener onClickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(body);
        builder.setView(view);
        builder.setPositiveButton(R.string.dialog_ok, onClickListener);
        builder.setCancelable(false);
        builder.show();
    }

    public static void showAlertDialog(String title, String body, DialogInterface.OnClickListener onClickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(body);
        builder.setPositiveButton(R.string.dialog_ok, onClickListener);
        builder.setCancelable(false);
        builder.show();
    }
}
