package eu.darkh.library.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Aleksander_Braula on 18-Jan-15.
 */
public class NetworkUtils {

    public static boolean hasNetworkConnection() {
        ConnectivityManager cm = (ConnectivityManager)UiUtils.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        Log.d("MT", "Has network connection: " + isConnected);

        return isConnected;
    }

    public static String tryToDownloadDataFromURL(String urlStr) {
        try {
            return downloadDataFromURL(urlStr);
        } catch (Exception e) {
            Log.e("MT", "Connection problem", e);
        }
        return "";
    }

    private static String downloadDataFromURL(String urlStr) throws IOException {
        Log.d("MT", "Download data from: " + urlStr);

        URL url = new URL(urlStr);

        HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
        InputStream inputStream = urlConnection.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));

        StringBuilder sb = new StringBuilder();

        String line;
        while ((line = br.readLine()) != null) {
            sb.append(line).append("\n");
        }

        inputStream.close();
        urlConnection.disconnect();

        return sb.toString();
    }
}

